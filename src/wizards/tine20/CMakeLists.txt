# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: BSD-3-Clause

install ( FILES tine20wizard.desktop tine20wizard.es tine20wizard.ui DESTINATION ${KDE_INSTALL_DATADIR}/akonadi/accountwizard/tine20 ) 
